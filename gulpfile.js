const gulp = require('gulp');
const browserSync = require('browser-sync');
const reload = browserSync.reload;
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const rollup = require('gulp-rollup');

gulp.task('rollup', () => gulp.src('./src/js/*.js')
    .pipe(rollup({
        entry: './src/js/index.js'
    }))
    .pipe(gulp.dest('./build')));


// Compile the Sass
gulp.task('sass', () => gulp.src('src/sass/*.scss')
    // transpile all Sass to CSS
    .pipe(sass({
        includePaths: ['src/sass'],
        outputStyle: 'compressed'
    }))
    // apply CSS browser compatibility prefixes
    .pipe(autoprefixer({
        browsers: [
            'last 4 versions',
            'chrome 29',
            'safari 5',
            'opera 12.1',
            'ios 6',
            'android 4'
        ],
        remove: false
    }))
    .pipe(gulp.dest('build')));

gulp.task('sass-watch', ['sass'], reload);
gulp.task('rollup-watch', ['rollup'], reload);

// launch the development web server
gulp.task('dev', ['sass', 'rollup'], () => {
    browserSync.init({
        server: { baseDir: '.' }
    });

    gulp.watch('src/js/*.js', ['rollup-watch']);
    gulp.watch('src/sass/*.scss', ['sass-watch']);
});

// ---- top level tasks ----
gulp.task('default', ['dev']);