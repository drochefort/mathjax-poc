const fs = require('fs');
const jsdom = require('jsdom').jsdom;
const mathjax = require('mathjax-node/lib/mj-page.js');

var content = fs.readFileSync('../samples.html');

var document = jsdom(content);

// Remove MathJax
mathjax.start();

mathjax.typeset({
    html: document.body.innerHTML,
    renderer: 'CommonHTML',
    inputs: ['MathML'],
}, result => {
    document.body.innerHTML = result.html;
    fs.writeFileSync('../html-css.html', document.documentElement.outerHTML);
});
